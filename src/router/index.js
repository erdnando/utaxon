import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/onboarding',
    name: 'registro',
    component: () => import(/* webpackChunkName: "registro" */ '../views/Registro.vue')
  },
  {
    path: '/onboarding/proceso',
    name: 'proceso',
    component: () => import(/* webpackChunkName: "proceso" */ '../views/Proceso.vue')
  },
  {
    path: '/onboarding/proceso2',
    name: 'proceso2',
    component: () => import(/* webpackChunkName: "proceso" */ '../views/Proceso2.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
