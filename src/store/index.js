import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    dialog0:false,
    dialog1:false,
    dialog2:false,
    dialog3:false,
    dialog4:false,
    dialog5:false,
    dialog6:false,
    dialog7:false,
    dialog8:false,
    dialog9:false,
    dialog10:false,
    dialog11:false,
    dialog12:false,
    dialog13:false,
    bTengoRFC:true,
    cliente:{
      nombre:'',
      email:'',
      password:'',
      telefono:'',
    },
    mensajes : {
      procesoHeader:'Bienvenido al proceso de onboarding, por favor contesta las siguientes preguntas:',
    },
    proceso:{
       dialogPropuesta:false,
       dialogVistaFinal:false,
       step01: {//ingresos
        ingresos: [
          { select:false,categoria:'Autompleo', SUBTOTAL:195, title: '<b>Autompleo</b> Ganancias como comerciante, profesional independiente,contratista', src: require('@/assets/AUTOEMPLEO.png'), flex: 12 },
          { select:false,categoria:'Empleado', SUBTOTAL:75, title: '<b>Empleado</b> Ganancias como empleado asalariado', src: require('@/assets/EMPLEADO.png'), flex: 6 },
          { select:false,categoria:'Pensionado', SUBTOTAL:75, title: '<b>Pensionado</b> Obtienes ganancias como pensionado', src: require('@/assets/PENSIONADOS.png'), flex: 6 },
          { select:false,categoria:'Empresario', SUBTOTAL:1200 ,title: '<b>Empresario</b> Ganancias o perdidas de inversiones financieras', src: require('@/assets/EMPRESARIO.png'), flex: 6 },
          { select:false,categoria:'Ingresos extranjeros',SUBTOTAL:1500, title: '<b>Ingresos extranjeros</b> Cualquier ingreso recibo del extranjero', src: require('@/assets/INGRESOS-EXTRANJEROS.png'), flex: 6 },
          { select:false,categoria:'Otros ingresos',SUBTOTAL:100, title: '<b>Otros ingresos</b> Obtienes cualquier otro ingreso', src: require('@/assets/OTROS-INGRESOS.png'), flex: 6 },
          { select:false,categoria:'Inversiones',SUBTOTAL:75, title: '<b>Inversiones</b> Ganancias o perdidas de inversiones financieras', src: require('@/assets/INVERSIONES.png'), flex: 6 },
        ]
       },
       domicilio: {//domicilio
        pais: 'México',
        estado:'',
        municipio:'',
       },
       propiedades: {
        cantidad: '0',
       },
       diasRentados: {
        cantidad: '0',
       },
       usuariosRentando: {
        cantidad: '0',
       },
       comentarios: {
        comentario: '',
       },
       datosRFC: {
        a: true,
        b: false,
        rfc:'',
        c:false,
        d:true
       },
       datosFiel: {
        a: false,
        b: true,
        c:false,
        d:true
       },
    },
    clasificacion:{
      MONTO_BASE:205.00,
      COMISION_COBRO:0.035,
      TASA_IVA:16,
      PAIS_DIFERENTE_MEXICO:2000,
      PROPIEDADES_EN_RENTA:50,
      DIAS_EN_RENTA:0,
      USUARIOS_EN_RENTA:5,
      OTRO_INGRESO_ADICIONAL:50,
      ELEMENTOS:[
        {
          DESCRIPCION:'AUTO_EMPLEO',
          CLAVE:'AE',
          CANTIDAD:1,
          COSTO_UNITARIO:195,
          SUBTOTAL:195,
        },
        {
          DESCRIPCION:'EMPLEADO',
          CLAVE:'E',
          CANTIDAD:1,
          COSTO_UNITARIO:75,
          SUBTOTAL:75,
        },
        {
          DESCRIPCION:'PENSIONADO',
          CLAVE:'P',
          CANTIDAD:1,
          COSTO_UNITARIO:75,
          SUBTOTAL:75,
        },
        {
          DESCRIPCION:'EMPRESARIO',
          CLAVE:'EM',
          CANTIDAD:1,
          COSTO_UNITARIO:1200,
          SUBTOTAL:1200,
        },
        {
          DESCRIPCION:'INGRESOS_EXTRANJEROS',
          CLAVE:'IE',
          CANTIDAD:1,
          COSTO_UNITARIO:1500,
          SUBTOTAL:1500,
        },
        {
          DESCRIPCION:'OTROS_INGRESOS',
          CLAVE:'OI',
          CANTIDAD:1,
          COSTO_UNITARIO:100,
          SUBTOTAL:100,
        },
      ],
    }
  },
  getters: {
    getIngresos: state => {
      return state.proceso.step01.ingresos;
    },
    getCalculoTotal: state => {

      var clasificacion= 0;
      var subTotal = 0;
      var iva = 0;
      var comision_cobro=0.0;
      var comisiones = 0;
      var total =0;
      var totalSinComisiones=0;
      var pais = state.proceso.domicilio.pais;

      state.proceso.step01.ingresos.forEach(ingreso => {
        if(ingreso.select)
           clasificacion+=ingreso.SUBTOTAL;
      });
      
      if(pais != 'México')
      clasificacion+=state.clasificacion.PAIS_DIFERENTE_MEXICO;
      
      clasificacion += (state.proceso.propiedades.cantidad * state.clasificacion.PROPIEDADES_EN_RENTA);
     // clasificacion += (state.proceso.diasRentados.cantidad * state.clasificacion.DIAS_EN_RENTA);
      clasificacion += (state.proceso.usuariosRentando.cantidad * state.clasificacion.USUARIOS_EN_RENTA);
      //OTRO_INGRESO_ADICIONAL
      console.log('clasificacion');
      console.log(clasificacion);
      subTotal = state.clasificacion.MONTO_BASE + clasificacion;
      console.log('subTotal');
      console.log(subTotal);
      
      iva = subTotal * state.clasificacion.TASA_IVA/100 ;
      console.log('iva');
      console.log(iva);
      comision_cobro = (subTotal + iva) * state.clasificacion.COMISION_COBRO;
      console.log('comision_cobro');
      console.log(comision_cobro);
      total = subTotal+iva+comision_cobro;
      console.log('total');
      console.log(total);
      //comisiones = iva+comision_cobro;
      //totalSinComisiones = total - comisiones;

      return Math.round(total);
     
    }
  },
  mutations: {
    setNombre(state, nombre) {
      state.cliente.nombre = nombre;
  },
  },
  actions: {
  
  },
  modules: {

  }
});
